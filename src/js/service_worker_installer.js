
// code idea from https://github.com/jakearchibald/wittr
export default function ServiceWorkerInstaller() {
    this._registerServiceWorker();
}

ServiceWorkerInstaller.prototype._registerServiceWorker = function() {
    if (!navigator.serviceWorker) return;
    console.log("installing service worker...")
    navigator.serviceWorker.register('/sw.js').then(function() {
        // if (!navigator.serviceWorker.controller) {
        //     return;
        // }
        console.log("service worker installed")
    });

    let isRefreshing;
    navigator.serviceWorker.addEventListener('controllerchange', function() {
        if (isRefreshing) return;
        window.location.reload();
        isRefreshing = true;
    });
};