
const staticCacheName_noVersion = 'restaurant-reviews-static-v';
const version = '10'
let staticCacheName = staticCacheName_noVersion + version;
import idb from 'idb'

const allCaches = [
    staticCacheName
];

const origin = 'http://localhost:8000';
const databaseOrigin = 'http://localhost:1337';
self.addEventListener('install', function(event) {
        console.log('installing cache files')
    event.waitUntil(
        caches.open(staticCacheName).then(function(cache) {
            return cache.addAll([
                '/css/styles.css',
                'img/1.webp',
                'img/2.webp',
                'img/3.webp',
                'img/4.webp',
                'img/5.webp',
                'img/6.webp',
                'img/7.webp',
                'img/8.webp',
                'img/9.webp',
                'img/10.webp',
                'img/icon_192.png',
                'img/icon_512.png',
                '/index.html',
                '/install_sw.js',
                '/js/dbhelper.js',
                '/js/main.js',
                '/js/restaurant_info.js',
                '/restaurant.html',
                '/manifest.json'
            ]);

        }).then(() => {
            fetch(`${databaseOrigin}/restaurants`).then((response) => {
                console.log("fetching data from web...")

                // put all the information into the db upon install
                updateIdb(null, response)
    
            })
        })
    );
});

self.addEventListener('activate', function(event) {

    // update caches on the browser to make sure we don't have duplicates and old versions
    event.waitUntil(
        caches.keys().then(function(cacheNames) {
            return Promise.all(
                cacheNames.filter(function(cacheName) {
                    return cacheName.startsWith('restaurant-reviews-') &&
                        !allCaches.includes(cacheName) && !cacheName.endsWith(version);
                }).map(function(cacheName) {
                    return caches.delete(cacheName);
                })
            );
        })
    );
});

self.addEventListener('fetch', function(event) {
    const url = new URL(event.request.url);
    const fetchOrigin = url.origin;

    if ( url.pathname === '/' ) {
        event.respondWith(caches.match('/index.html'));
        return;
    }

    if ( url.pathname.startsWith('/restaurant.html') ) {
        event.respondWith(caches.match('/restaurant.html'));
        return;
    }

    if ( fetchOrigin.startsWith(origin) || fetchOrigin.startsWith('fonts') ) {
        console.log("fetching from cache")
        event.respondWith(serveAsset(event.request));
        return;
    }

    if ( fetchOrigin.startsWith(databaseOrigin) ) {
        console.log("Getting for indexeddb")
        event.respondWith(serveRestaurantData(event.request));
        return;
    }

    // only go the the network if it isn't a 'localhost' origin, e.g/ getting a map
   // console.log(event.request)
    event.respondWith(
        fetch(event.request).catch((error) => {
            console.log("Error with fetching external request.")
        })
    );


});



function serveAsset(request) {
    var url = request.url;
    //console.log("local", request)

        return caches.match(url).then(function(response) {
            if (response) {
                return response;
            }

            return fetch(request)

        });

}

function serveRestaurantData (request) {
    

        const url = request.url;

        let id;
        url.endsWith("restaurants") ? id = null : id = url.split("/")[4];

        return getOfflineData(id).then( (response) => {
            
            if (response) return response

            fetch(url).then((response) => {
                console.log("fetching data from web...")
                let responseClone = response.clone();
                
                updateIdb(id, responseClone)
    
                return response
            })
            .catch(() => {
                console.log("No way to get the data... sorry!")
                // return getOfflineData(id)
            })
        })
   
    

    
}

function updateIdb(id, response) {
    
    idb.open('restaurants', 1, function(upgradeDb) {
        // create the database if not
        console.log("database doesn't exist, now creating database...")
        upgradeDb.createObjectStore('restaurants');
        
    }).then((db) => {
        // console.log("Updating db...")
        
        id ? 
        //replaceById(id, responseClone, db) 
        
        response.json().then((restaurant) => {
            const tx = db.transaction('restaurants', 'readwrite');
            const store = tx.objectStore('restaurants');
            store.put(restaurant, `${id}`)
            return tx.complete;
        })
        : 
        //replaceAll(responseClone, db)
        response.json().then((restaurants) => {
            restaurants.forEach((restaurant) => {
                const tx = db.transaction('restaurants', 'readwrite');
                const store = tx.objectStore('restaurants');
                store.put(restaurant, `${restaurant.id}`)
                return tx.complete
            })
        })
        
});
}



function getOfflineData(id) {
    const keyVal = 'restaurants'
    let dbPromise = idb.open(keyVal);

    if (id) {
        return dbPromise.then(db => {
            console.log('db: ', db)
            return db.transaction(keyVal).objectStore(keyVal).get(id);
          }).then(restaurant => new Response(JSON.stringify(restaurant),  { "status" : 200 , "statusText" : `Custom Response: Get Restaurant ${restaurant.id}` }))
          .catch((e) => {
              console.log("error in db", e)
          })
        } else {
        console.log("offline 'all' request...")

        return dbPromise.then(db => {
            console.log('db', db)
            return db.transaction(keyVal).objectStore(keyVal).getAll();
          }).then(restaurants => new Response(JSON.stringify(restaurants),  { "status" : 200 , "statusText" : "Custom Response: Get All Restaurants" }))
          .catch((e) => {
            console.log("error in db", e)
        })
    }  
}
