const path = require('path')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const glob = require('glob')
const ImageminPlugin = require('imagemin-webpack-plugin').default

const mode = 'development'

const config = {
    plugins: [
        new ImageminPlugin( {
            externalImages: {
                context: 'src/imgs/', // Important! This tells the plugin where to "base" the paths at
                sources: glob.sync( 'src/imgs/*.webp' ),
                destination: './public/img/' 
            }
        })
    ],
    optimization: {
        minimize: true,
        minimizer: [
          new UglifyJsPlugin({
            test: /\.js($|\?)/i,
            uglifyOptions: {
                keep_classnames: true,
                keep_fnames: true
            }
          })
        ]
    },
    module: {
        rules: [
            {
                loader: 'babel-loader',
                test: /\.js$/,
                exclude: /node_modules/
            }
        ]
    },
    devtool: 'inline-source-map',
    devServer: {
        compress: true,
        contentBase: path.join(__dirname, 'public'),
        historyApiFallback: true,
        port: 8000
    }
  
}

// const imgConfig = Object.assign({}, {
//     mode,
    
    
// })

const serverWorkerConfig= Object.assign({}, config, {
    mode,
    entry: {
        install_sw: './src/js/service_worker_entry.js',
        sw: './src/js/service-worker.js'
    },
    output: {
        path: path.join(__dirname, 'public'),
        filename: '[name].js'
    }
})

const mainJavascriptConfig = Object.assign({}, {
    mode,
    entry: {
        
        main: './src/js/main.js',
        dbhelper: './src/js/dbhelper.js',
        restaurant_info: './src/js/restaurant_info.js',
        index_controller: './src/js/index_controller.js'
    },
    output: {
        path: path.join(__dirname, 'public', 'js'),
        filename: '[name].js'
    }
})

module.exports = [
     serverWorkerConfig
]
