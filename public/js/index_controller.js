/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = IndexController;
function IndexController(container) {
    console.log("created IndexController");
    this._container = container;
    this._registerServiceWorker();

    var indexController = this;
}

IndexController.prototype._registerServiceWorker = function () {
    if (!navigator.serviceWorker) return;

    var indexController = this;

    navigator.serviceWorker.register('/js/sw.js').then(function (reg) {
        if (!navigator.serviceWorker.controller) {
            return;
        }
    });

    // Ensure refresh is only called once.
    // This works around a bug in "force update on reload".
    var refreshing;
    navigator.serviceWorker.addEventListener('controllerchange', function () {
        if (refreshing) return;
        window.location.reload();
        refreshing = true;
    });
};

IndexController.prototype._cleanImageCache = function () {
    return this._dbPromise.then(function (db) {
        if (!db) return;

        var imagesNeeded = [];

        var tx = db.transaction('restaurant-reviews');
        return tx.objectStore('restaurant-reviews').getAll().then(function (reviews) {

            return caches.open('restaurant-reviews-content-imgs');
        }).then(function (cache) {
            return cache.keys().then(function (requests) {
                requests.forEach(function (request) {
                    var url = new URL(request.url);
                    if (!imagesNeeded.includes(url.pathname)) cache.delete(request);
                });
            });
        });
    });
};

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(0);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZDRkMjZiMWY1OTZiZjdlMjBiMzUiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL2luZGV4X2NvbnRyb2xsZXIuanMiXSwibmFtZXMiOlsiSW5kZXhDb250cm9sbGVyIiwiY29udGFpbmVyIiwiY29uc29sZSIsImxvZyIsIl9jb250YWluZXIiLCJfcmVnaXN0ZXJTZXJ2aWNlV29ya2VyIiwiaW5kZXhDb250cm9sbGVyIiwicHJvdG90eXBlIiwibmF2aWdhdG9yIiwic2VydmljZVdvcmtlciIsInJlZ2lzdGVyIiwidGhlbiIsInJlZyIsImNvbnRyb2xsZXIiLCJyZWZyZXNoaW5nIiwiYWRkRXZlbnRMaXN0ZW5lciIsIndpbmRvdyIsImxvY2F0aW9uIiwicmVsb2FkIiwiX2NsZWFuSW1hZ2VDYWNoZSIsIl9kYlByb21pc2UiLCJkYiIsImltYWdlc05lZWRlZCIsInR4IiwidHJhbnNhY3Rpb24iLCJvYmplY3RTdG9yZSIsImdldEFsbCIsInJldmlld3MiLCJjYWNoZXMiLCJvcGVuIiwiY2FjaGUiLCJrZXlzIiwicmVxdWVzdHMiLCJmb3JFYWNoIiwicmVxdWVzdCIsInVybCIsIlVSTCIsImluY2x1ZGVzIiwicGF0aG5hbWUiLCJkZWxldGUiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7a0JDM0R3QkEsZTtBQUFULFNBQVNBLGVBQVQsQ0FBeUJDLFNBQXpCLEVBQW9DO0FBQy9DQyxZQUFRQyxHQUFSLENBQVkseUJBQVo7QUFDQSxTQUFLQyxVQUFMLEdBQWtCSCxTQUFsQjtBQUNBLFNBQUtJLHNCQUFMOztBQUVBLFFBQU1DLGtCQUFrQixJQUF4QjtBQUNIOztBQUVETixnQkFBZ0JPLFNBQWhCLENBQTBCRixzQkFBMUIsR0FBbUQsWUFBVztBQUMxRCxRQUFJLENBQUNHLFVBQVVDLGFBQWYsRUFBOEI7O0FBRTlCLFFBQUlILGtCQUFrQixJQUF0Qjs7QUFFQUUsY0FBVUMsYUFBVixDQUF3QkMsUUFBeEIsQ0FBaUMsV0FBakMsRUFBOENDLElBQTlDLENBQW1ELFVBQVNDLEdBQVQsRUFBYztBQUM3RCxZQUFJLENBQUNKLFVBQVVDLGFBQVYsQ0FBd0JJLFVBQTdCLEVBQXlDO0FBQ3JDO0FBQ0g7QUFDSixLQUpEOztBQU1BO0FBQ0E7QUFDQSxRQUFJQyxVQUFKO0FBQ0FOLGNBQVVDLGFBQVYsQ0FBd0JNLGdCQUF4QixDQUF5QyxrQkFBekMsRUFBNkQsWUFBVztBQUNwRSxZQUFJRCxVQUFKLEVBQWdCO0FBQ2hCRSxlQUFPQyxRQUFQLENBQWdCQyxNQUFoQjtBQUNBSixxQkFBYSxJQUFiO0FBQ0gsS0FKRDtBQUtILENBbkJEOztBQXlCQWQsZ0JBQWdCTyxTQUFoQixDQUEwQlksZ0JBQTFCLEdBQTZDLFlBQVc7QUFDcEQsV0FBTyxLQUFLQyxVQUFMLENBQWdCVCxJQUFoQixDQUFxQixVQUFTVSxFQUFULEVBQWE7QUFDckMsWUFBSSxDQUFDQSxFQUFMLEVBQVM7O0FBRVQsWUFBSUMsZUFBZSxFQUFuQjs7QUFFQSxZQUFJQyxLQUFLRixHQUFHRyxXQUFILENBQWUsb0JBQWYsQ0FBVDtBQUNBLGVBQU9ELEdBQUdFLFdBQUgsQ0FBZSxvQkFBZixFQUFxQ0MsTUFBckMsR0FBOENmLElBQTlDLENBQW1ELFVBQVNnQixPQUFULEVBQWtCOztBQUd4RSxtQkFBT0MsT0FBT0MsSUFBUCxDQUFZLGlDQUFaLENBQVA7QUFDSCxTQUpNLEVBSUpsQixJQUpJLENBSUMsVUFBU21CLEtBQVQsRUFBZ0I7QUFDcEIsbUJBQU9BLE1BQU1DLElBQU4sR0FBYXBCLElBQWIsQ0FBa0IsVUFBU3FCLFFBQVQsRUFBbUI7QUFDeENBLHlCQUFTQyxPQUFULENBQWlCLFVBQVNDLE9BQVQsRUFBa0I7QUFDL0Isd0JBQUlDLE1BQU0sSUFBSUMsR0FBSixDQUFRRixRQUFRQyxHQUFoQixDQUFWO0FBQ0Esd0JBQUksQ0FBQ2IsYUFBYWUsUUFBYixDQUFzQkYsSUFBSUcsUUFBMUIsQ0FBTCxFQUEwQ1IsTUFBTVMsTUFBTixDQUFhTCxPQUFiO0FBQzdDLGlCQUhEO0FBSUgsYUFMTSxDQUFQO0FBTUgsU0FYTSxDQUFQO0FBWUgsS0FsQk0sQ0FBUDtBQW1CSCxDQXBCRCxDIiwiZmlsZSI6ImpzL2luZGV4X2NvbnRyb2xsZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSA1KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCBkNGQyNmIxZjU5NmJmN2UyMGIzNSIsIlxyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gSW5kZXhDb250cm9sbGVyKGNvbnRhaW5lcikge1xyXG4gICAgY29uc29sZS5sb2coXCJjcmVhdGVkIEluZGV4Q29udHJvbGxlclwiKVxyXG4gICAgdGhpcy5fY29udGFpbmVyID0gY29udGFpbmVyO1xyXG4gICAgdGhpcy5fcmVnaXN0ZXJTZXJ2aWNlV29ya2VyKCk7XHJcblxyXG4gICAgY29uc3QgaW5kZXhDb250cm9sbGVyID0gdGhpcztcclxufVxyXG5cclxuSW5kZXhDb250cm9sbGVyLnByb3RvdHlwZS5fcmVnaXN0ZXJTZXJ2aWNlV29ya2VyID0gZnVuY3Rpb24oKSB7XHJcbiAgICBpZiAoIW5hdmlnYXRvci5zZXJ2aWNlV29ya2VyKSByZXR1cm47XHJcblxyXG4gICAgdmFyIGluZGV4Q29udHJvbGxlciA9IHRoaXM7XHJcblxyXG4gICAgbmF2aWdhdG9yLnNlcnZpY2VXb3JrZXIucmVnaXN0ZXIoJy9qcy9zdy5qcycpLnRoZW4oZnVuY3Rpb24ocmVnKSB7XHJcbiAgICAgICAgaWYgKCFuYXZpZ2F0b3Iuc2VydmljZVdvcmtlci5jb250cm9sbGVyKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICAvLyBFbnN1cmUgcmVmcmVzaCBpcyBvbmx5IGNhbGxlZCBvbmNlLlxyXG4gICAgLy8gVGhpcyB3b3JrcyBhcm91bmQgYSBidWcgaW4gXCJmb3JjZSB1cGRhdGUgb24gcmVsb2FkXCIuXHJcbiAgICB2YXIgcmVmcmVzaGluZztcclxuICAgIG5hdmlnYXRvci5zZXJ2aWNlV29ya2VyLmFkZEV2ZW50TGlzdGVuZXIoJ2NvbnRyb2xsZXJjaGFuZ2UnLCBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAocmVmcmVzaGluZykgcmV0dXJuO1xyXG4gICAgICAgIHdpbmRvdy5sb2NhdGlvbi5yZWxvYWQoKTtcclxuICAgICAgICByZWZyZXNoaW5nID0gdHJ1ZTtcclxuICAgIH0pO1xyXG59O1xyXG5cclxuXHJcblxyXG5cclxuXHJcbkluZGV4Q29udHJvbGxlci5wcm90b3R5cGUuX2NsZWFuSW1hZ2VDYWNoZSA9IGZ1bmN0aW9uKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuX2RiUHJvbWlzZS50aGVuKGZ1bmN0aW9uKGRiKSB7XHJcbiAgICAgICAgaWYgKCFkYikgcmV0dXJuO1xyXG5cclxuICAgICAgICB2YXIgaW1hZ2VzTmVlZGVkID0gW107XHJcblxyXG4gICAgICAgIHZhciB0eCA9IGRiLnRyYW5zYWN0aW9uKCdyZXN0YXVyYW50LXJldmlld3MnKTtcclxuICAgICAgICByZXR1cm4gdHgub2JqZWN0U3RvcmUoJ3Jlc3RhdXJhbnQtcmV2aWV3cycpLmdldEFsbCgpLnRoZW4oZnVuY3Rpb24ocmV2aWV3cykge1xyXG5cclxuXHJcbiAgICAgICAgICAgIHJldHVybiBjYWNoZXMub3BlbigncmVzdGF1cmFudC1yZXZpZXdzLWNvbnRlbnQtaW1ncycpO1xyXG4gICAgICAgIH0pLnRoZW4oZnVuY3Rpb24oY2FjaGUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGNhY2hlLmtleXMoKS50aGVuKGZ1bmN0aW9uKHJlcXVlc3RzKSB7XHJcbiAgICAgICAgICAgICAgICByZXF1ZXN0cy5mb3JFYWNoKGZ1bmN0aW9uKHJlcXVlc3QpIHtcclxuICAgICAgICAgICAgICAgICAgICB2YXIgdXJsID0gbmV3IFVSTChyZXF1ZXN0LnVybCk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFpbWFnZXNOZWVkZWQuaW5jbHVkZXModXJsLnBhdGhuYW1lKSkgY2FjaGUuZGVsZXRlKHJlcXVlc3QpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSk7XHJcbn07XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2pzL2luZGV4X2NvbnRyb2xsZXIuanMiXSwic291cmNlUm9vdCI6IiJ9